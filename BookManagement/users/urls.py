from django.urls import path, include

from rest_framework.routers import DefaultRouter

from . import views

user_login_router = DefaultRouter()
user_login_router.register("login", views.UserLoginViewset, basename="login")

user_signup_router = DefaultRouter()
user_signup_router.register("signup", views.UserSignUpViewset, basename="signup")

urlpatterns = [
    path("", include(user_login_router.urls)),
    path("", include(user_signup_router.urls)),
]
