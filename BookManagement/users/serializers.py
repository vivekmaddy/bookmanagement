from django.contrib.auth.models import User

from rest_framework import serializers


# Create your models here.


class UserListingSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ("id", "username", "email", "first_name", "last_name")


class UserSignUpSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ("id", "username", "password",)

    def create(self, validated_data):
        return User.objects.create_user(**validated_data)
