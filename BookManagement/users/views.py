from django.contrib.auth import authenticate
from django.contrib.auth.models import User

from rest_framework import viewsets, status
from rest_framework.response import Response

from common.utils import get_tokens_for_user
from .serializers import UserListingSerializer, UserSignUpSerializer


# Create your views here.


class UserLoginViewset(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserListingSerializer
    permission_classes = []
    http_method_names = ['post']

    response = {
        "status": status.HTTP_200_OK,
        "message": "Login Success!",
        "data": None
    }

    def create(self, request, *args, **kwargs):
        response = self.response.copy()
        try:
            username = request.data["username"]
            password = request.data["password"]
            user = authenticate(username=username, password=password)
            if user is None:
                raise Exception("Incorrect username / password!")
            tokens_dict = get_tokens_for_user(user)
            response["data"] = {"token": tokens_dict, "user": self.get_serializer(user).data}
        except Exception as err:
            response["status"] = status.HTTP_400_BAD_REQUEST
            response["message"] = str(err)
        return Response(response, status=response["status"])


class UserSignUpViewset(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSignUpSerializer
    permission_classes = []
    http_method_names = ['post']

    response = {
        "status": status.HTTP_201_CREATED,
        "message": "SignUp Success!",
        "data": None
    }

    def create(self, request, *args, **kwargs):
        response = self.response.copy()
        try:
            serializer = self.get_serializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            serializer.save()
        except Exception as err:
            response["status"] = status.HTTP_400_BAD_REQUEST
            response["message"] = str(err)
        return Response(response, status=response["status"])
