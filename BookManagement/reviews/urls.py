from rest_framework.routers import DefaultRouter

from django.urls import path, include

from . import views

book_review_router = DefaultRouter()
book_review_router.register("book", views.ReviewBookViewset, basename="review-book")

author_review_router = DefaultRouter()
author_review_router.register("author", views.ReviewAuthorViewset, basename="review-author")

reviews_router = DefaultRouter()
reviews_router.register("", views.ReviewsViewset, basename="reviews")

urlpatterns = [
    path("user-review/", include(book_review_router.urls)),
    path("user-review/", include(author_review_router.urls)),
    path("", include(reviews_router.urls)),

]
