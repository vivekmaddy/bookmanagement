from rest_framework import serializers

from .models import Reviews
from books.serializers import BooksListingSerializer
from authors.serializers import AuthorsListingSerializer


class ReviewsSerializer(serializers.ModelSerializer):
    author = serializers.SerializerMethodField()
    book = serializers.SerializerMethodField()

    class Meta:
        model = Reviews
        fields = "__all__"

    def get_author(self, obj):
        return AuthorsListingSerializer(obj.author).data

    def get_book(self, obj):
        return BooksListingSerializer(obj.book).data


class ReviewAuthorSerializer(serializers.ModelSerializer):
    def validate(self, attrs):
        if not attrs["author"]:
            raise serializers.ValidationError("author required!")
        return attrs

    class Meta:
        model = Reviews
        exclude = ("book",)


class ReviewBookSerializer(serializers.ModelSerializer):

    def validate(self, attrs):
        if not attrs["book"]:
            raise serializers.ValidationError("book required!")
        return attrs

    class Meta:
        model = Reviews
        exclude = ("author",)


class ReviewBookListingSerializer(serializers.ModelSerializer):
    book = serializers.SerializerMethodField()

    class Meta:
        model = Reviews
        exclude = ("author",)

    def get_book(self, obj):
        return BooksListingSerializer(obj.book).data


class ReviewAuthorListingSerializer(serializers.ModelSerializer):
    author = serializers.SerializerMethodField()

    class Meta:
        model = Reviews
        exclude = ("book",)

    def get_author(self, obj):
        return AuthorsListingSerializer(obj.author).data
