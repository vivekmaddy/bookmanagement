from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver
from django.db.models import Sum

from .models import Reviews
from books.models import Books
from authors.models import Authors


@receiver(post_save, sender=Reviews, dispatch_uid="update_total_rating_avg_rating_post_save")
@receiver(post_delete, sender=Reviews, dispatch_uid="update_total_rating_avg_rating_post_delete")
def update_total_rating_avg_rating(sender, instance, **kwargs):
    author = instance.author
    book = instance.book

    def update_rating(target_object, reviews):
        total_rating = reviews.count()
        avg_rating = reviews.aggregate(Sum("rating", default=0))['rating__sum'] / total_rating

        target_object.total_rating = total_rating
        target_object.avg_rating = round(avg_rating, 1)
        target_object.save()

    try:
        if author:
            reviews = Reviews.objects.filter(author=author.id)
            author_obj = Authors.objects.get(id=author.id)

            update_rating(author_obj, reviews)
        elif book:
            reviews = Reviews.objects.filter(book=book)
            book_obj = Books.objects.get(id=book.id)
            update_rating(book_obj, reviews)

    except Exception as err:
        print("update_total_rating_avg_rating signal err", err)
