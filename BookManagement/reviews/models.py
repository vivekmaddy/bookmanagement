from django.contrib.auth.models import User
from django.db import models
from django.core.exceptions import ValidationError

from authors.models import Authors
from books.models import Books


# Create your models here.

class Base(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


def validate_reviews_rating(value):
    if value > 5:
        raise ValidationError("Rating must be 1-5!")


class Reviews(Base):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    author = models.ForeignKey(Authors, on_delete=models.CASCADE, blank=True, null=True)
    book = models.ForeignKey(Books, on_delete=models.CASCADE, blank=True, null=True)
    rating = models.FloatField(validators=[validate_reviews_rating])
    review = models.TextField(blank=True, null=True)

    def __str__(self):
        return f"{self.user} ==> {self.rating} ==> {self.review}"

    def save(self, *args, **kwargs):
        author = self.author
        book = self.book
        if author and book:
            raise ValidationError("Review Failed! You can only review author or book at a time!")
        elif author is None and book is None:
            raise ValidationError("Author and Book atleast one required, Both cannot be empty!")
        super(Reviews, self).save(*args, **kwargs)
