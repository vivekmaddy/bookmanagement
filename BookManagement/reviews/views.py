from rest_framework import viewsets, status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from .models import Reviews
from .serializers import ReviewAuthorSerializer, ReviewAuthorListingSerializer, ReviewBookSerializer, \
    ReviewBookListingSerializer, ReviewsSerializer


# Create your views here.
class ReviewsViewset(viewsets.ModelViewSet):
    queryset = Reviews.objects.all()
    serializer_class = ReviewsSerializer
    permission_classes = [IsAuthenticated]
    http_method_names = ['get']

    response = {
        "status": status.HTTP_200_OK,
        "message": "Ok!",
        "data": None
    }

    def get_queryset(self):
        author = self.request.GET.get("author", False)
        book = self.request.GET.get("book", False)
        filter_dict = {}

        if author:
            filter_dict["author"] = author
        if book:
            filter_dict["book"] = book

        return self.queryset.filter(**filter_dict).order_by("-id")

    def list(self, request, *args, **kwargs):
        response = self.response.copy()
        try:
            response["data"] = self.get_serializer(self.get_queryset(), many=True).data
        except Exception as err:
            response["status"] = status.HTTP_400_BAD_REQUEST
            response["message"] = str(err)
        return Response(response, status=response["status"])


class ReviewBookViewset(viewsets.ModelViewSet):
    queryset = Reviews.objects.all()
    serializer_class = ReviewBookListingSerializer
    permission_classes = [IsAuthenticated]
    http_method_names = ['post', 'get', 'delete']

    response = {
        "status": status.HTTP_200_OK,
        "message": "Ok!",
        "data": None
    }

    def get_queryset(self):
        return self.queryset.filter(user_id=self.request.user.id).order_by("-id")

    def get_serializer_class(self):
        if self.request.method == "POST":
            return ReviewBookSerializer
        else:
            return self.serializer_class

    def create(self, request, *args, **kwargs):
        response = self.response.copy()
        try:
            payload = request.data.copy()
            payload["user"] = request.user.id
            qs = self.get_queryset().filter(book_id=payload["book"])
            if qs:
                serializer = self.get_serializer(qs.first(), data=payload, partial=True)
            else:
                serializer = self.get_serializer(data=payload)
            serializer.is_valid(raise_exception=True)
            serializer.save()

            response["status"] = status.HTTP_201_CREATED
            response["message"] = "Review added successfully!"
            response["data"] = serializer.data
        except Exception as err:
            response["status"] = status.HTTP_400_BAD_REQUEST
            response["message"] = str(err)
        return Response(response, status=response["status"])

    def list(self, request, *args, **kwargs):
        response = self.response.copy()
        try:
            response["data"] = self.get_serializer(self.get_queryset(), many=True).data
        except Exception as err:
            response["status"] = status.HTTP_400_BAD_REQUEST
            response["message"] = str(err)
        return Response(response, status=response["status"])

    def retrieve(self, request, *args, **kwargs):
        response = self.response.copy()
        try:
            response["data"] = self.get_serializer(self.get_object()).data
        except Exception as err:
            response["status"] = status.HTTP_400_BAD_REQUEST
            response["message"] = str(err)
        return Response(response, status=response["status"])

    def destroy(self, request, *args, **kwargs):
        response = self.response.copy()
        try:
            self.get_object().delete()
            response["message"] = "Deleted!"
        except Exception as err:
            response["status"] = status.HTTP_400_BAD_REQUEST
            response["message"] = str(err)
        return Response(response, status=response["status"])


class ReviewAuthorViewset(viewsets.ModelViewSet):
    queryset = Reviews.objects.all()
    serializer_class = ReviewAuthorListingSerializer
    permission_classes = [IsAuthenticated]
    http_method_names = ['post', 'get', 'delete']

    response = {
        "status": status.HTTP_200_OK,
        "message": "Ok!",
        "data": None
    }

    def get_queryset(self):
        return self.queryset.filter(user_id=self.request.user.id).order_by("-id")

    def get_serializer_class(self):
        if self.request.method == "POST":
            return ReviewAuthorSerializer
        else:
            return self.serializer_class

    def create(self, request, *args, **kwargs):
        response = self.response.copy()
        try:
            payload = request.data.copy()
            payload["user"] = request.user.id
            qs = self.get_queryset().filter(author_id=payload["author"])
            if qs:
                serializer = self.get_serializer(qs.first(), data=payload, partial=True)
            else:
                serializer = self.get_serializer(data=payload)
            serializer.is_valid(raise_exception=True)
            serializer.save()

            response["status"] = status.HTTP_201_CREATED
            response["message"] = "Review added successfully!"
            response["data"] = serializer.data
        except Exception as err:
            response["status"] = status.HTTP_400_BAD_REQUEST
            response["message"] = str(err)
        return Response(response, status=response["status"])

    def list(self, request, *args, **kwargs):
        response = self.response.copy()
        try:
            response["data"] = self.get_serializer(self.get_queryset(), many=True).data
        except Exception as err:
            response["status"] = status.HTTP_400_BAD_REQUEST
            response["message"] = str(err)
        return Response(response, status=response["status"])

    def retrieve(self, request, *args, **kwargs):
        response = self.response.copy()
        try:
            response["data"] = self.get_serializer(self.get_object()).data
        except Exception as err:
            response["status"] = status.HTTP_400_BAD_REQUEST
            response["message"] = str(err)
        return Response(response, status=response["status"])

    def destroy(self, request, *args, **kwargs):
        response = self.response.copy()
        try:
            self.get_object().delete()
            response["message"] = "Deleted!"
        except Exception as err:
            response["status"] = status.HTTP_400_BAD_REQUEST
            response["message"] = str(err)
        return Response(response, status=response["status"])
