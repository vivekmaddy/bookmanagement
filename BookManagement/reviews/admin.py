from django.contrib import admin
from .models import Reviews


# Register your models here.


class ReviewsAdmin(admin.ModelAdmin):
    list_display = ("id", "user", "author", "book", "rating", "review")


admin.site.register(Reviews, ReviewsAdmin)
