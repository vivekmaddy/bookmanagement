# Generated by Django 5.0.2 on 2024-02-12 13:09

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('books', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='books',
            old_name='Language',
            new_name='language',
        ),
    ]
