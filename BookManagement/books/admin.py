from django.contrib import admin
from .models import Books


class BooksAdmin(admin.ModelAdmin):
    list_display = ("id", "author", "title", "total_rating", "avg_rating")


admin.site.register(Books, BooksAdmin)
