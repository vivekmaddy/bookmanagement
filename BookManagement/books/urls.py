from django.urls import path, include

from rest_framework.routers import DefaultRouter

from . import views

book_router = DefaultRouter()
book_router.register("book", views.BooksViewset, basename="book")

urlpatterns = [
    path("", include(book_router.urls)),
]
