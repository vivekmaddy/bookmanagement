from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver

from books.models import Books


@receiver(post_save, sender=Books, dispatch_uid="update_total_number_of_books_post_save")
@receiver(post_delete, sender=Books, dispatch_uid="update_total_number_of_books_post_delete")
def update_total_number_of_books_post_save(sender, instance, **kwargs):
    try:
        author = instance.author
        author.no_of_books = Books.objects.filter(author=author).count()
        author.save()
    except Exception as err:
        print("update_total_number_of_books signals err", err)
