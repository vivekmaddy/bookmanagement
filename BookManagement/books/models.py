import uuid

from django.db import models

from authors.models import Authors


class Base(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Books(Base):
    author = models.ForeignKey(Authors, on_delete=models.CASCADE)
    title = models.CharField(max_length=250)
    description = models.TextField()
    cover_image = models.ImageField(upload_to="BookCoverImages/", blank=True, null=True)
    language = models.CharField(max_length=100)
    ISBN = models.CharField(max_length=100, blank=True, null=True)
    publisher = models.CharField(max_length=250)
    publication_dt = models.DateTimeField()
    total_pages = models.BigIntegerField()
    total_rating = models.BigIntegerField(default=0)
    avg_rating = models.FloatField(default=0)

    def __str__(self):
        return f"{self.title} => {self.author.get_full_name()}"

    def get_ISBN(self, start=0, end=-1):
        uuid_ = str(uuid.uuid4()).replace("-", "")
        return uuid_[start:end]

    def save(self, *args, **kwargs):
        if not self.ISBN:
            uuid_ = self.get_ISBN(end=6)
            while Books.objects.filter(ISBN=uuid_).exists():
                uuid_ = self.get_ISBN(5)
            self.ISBN = uuid_
        super(Books, self).save(*args, **kwargs)
