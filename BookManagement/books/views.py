from rest_framework import viewsets, status
from rest_framework.response import Response
from rest_framework.permissions import IsAdminUser

from .models import Books
from .serializers import BooksCreateSerializer, BooksListingSerializer


# Create your views here.


class BooksViewset(viewsets.ModelViewSet):
    queryset = Books.objects.all()
    serializer_class = BooksListingSerializer
    permission_classes = [IsAdminUser]
    http_method_names = ['post', 'get', 'patch', 'delete']

    response = {
        "status": status.HTTP_200_OK,
        "message": "Ok!",
        "data": None
    }

    def get_queryset(self):
        return self.queryset.order_by("-id")

    def get_serializer_class(self):
        if self.request.method in ["POST", "PATCH"]:
            return BooksCreateSerializer
        else:
            return self.serializer_class

    def create(self, request, *args, **kwargs):
        response = self.response.copy()
        try:
            serializer = self.get_serializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            serializer.save()
            response["status"] = status.HTTP_201_CREATED
            response["message"] = "Created!"
            response["data"] = serializer.data
        except Exception as err:
            response["status"] = status.HTTP_400_BAD_REQUEST
            response["message"] = str(err)
        return Response(response, status=response["status"])

    def partial_update(self, request, *args, **kwargs):
        response = self.response.copy()
        try:
            serializer = self.get_serializer(self.get_object(), data=request.data, partial=True)
            serializer.is_valid(raise_exception=True)
            serializer.save()
            response["message"] = "Updated!"
            response["data"] = serializer.data
        except Exception as err:
            response["status"] = status.HTTP_400_BAD_REQUEST
            response["message"] = str(err)
        return Response(response, status=response["status"])

    def list(self, request, *args, **kwargs):
        response = self.response.copy()
        try:
            response["data"] = self.get_serializer(self.get_queryset(), many=True).data
        except Exception as err:
            response["status"] = status.HTTP_400_BAD_REQUEST
            response["message"] = str(err)
        return Response(response, status=response["status"])

    def retrieve(self, request, *args, **kwargs):
        response = self.response.copy()
        try:
            response["data"] = self.get_serializer(self.get_object()).data
        except Exception as err:
            response["status"] = status.HTTP_400_BAD_REQUEST
            response["message"] = str(err)
        return Response(response, status=response["status"])

    def destroy(self, request, *args, **kwargs):
        response = self.response.copy()
        try:
            self.get_object().delete()
            response["message"] = "Deleted!"
        except Exception as err:
            response["status"] = status.HTTP_400_BAD_REQUEST
            response["message"] = str(err)
        return Response(response, status=response["status"])
