from rest_framework import serializers

from .models import Books


class BooksCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Books
        exclude = ("total_rating", "avg_rating", "ISBN")


class BooksListingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Books
        fields = "__all__"
