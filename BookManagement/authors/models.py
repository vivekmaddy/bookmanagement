from django.db import models


# Create your models here.

class Base(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Authors(Base):
    first_name = models.CharField(max_length=200)
    middle_name = models.CharField(max_length=200, blank=True, null=True)
    last_name = models.CharField(max_length=200)
    bio = models.TextField(blank=True, null=True)
    photo = models.ImageField(blank=True, null=True, upload_to="authorImages/")
    date_of_birth = models.DateField(blank=True, null=True)
    date_of_death = models.DateField(blank=True, null=True)
    nationality = models.CharField(max_length=100, blank=True, null=True)
    contact_info = models.TextField(blank=True, null=True)
    education_info = models.TextField(blank=True, null=True)
    total_rating = models.BigIntegerField(default=0)
    avg_rating = models.FloatField(default=0)
    no_of_books = models.BigIntegerField(default=0)

    def get_full_name(self):
        first_name = self.first_name
        middle_name = f" {self.middle_name}" if self.middle_name else ""
        last_name = f" {self.last_name}"
        return f"{first_name}{middle_name}{last_name}"

    def __str__(self):
        return f"{self.first_name} "
