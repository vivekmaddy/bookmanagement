from django.urls import path, include
from rest_framework.routers import DefaultRouter

from . import views

author_router = DefaultRouter()
author_router.register("author", views.AuthorsViewset, basename="author")

urlpatterns = [
    path("", include(author_router.urls)),
]
