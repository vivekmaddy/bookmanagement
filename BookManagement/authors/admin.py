from django.contrib import admin
from .models import Authors


# Register your models here.
class AuthorsAdmin(admin.ModelAdmin):
    list_display = ("id", "first_name", "last_name", "total_rating", "avg_rating")


admin.site.register(Authors, AuthorsAdmin)
