from rest_framework import serializers

from .models import Authors


class AuthorsCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Authors
        exclude = ("total_rating", "avg_rating", "no_of_books")


class AuthorsListingSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField()

    class Meta:
        model = Authors
        exclude = ("first_name", "middle_name", "last_name")

    def get_name(self, obj):
        return obj.get_full_name()
