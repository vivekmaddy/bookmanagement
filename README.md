# BookManagement

## About

Mandatory Tech stack: Python, Django, Postgres.
 
* Table to store authors (with a field for storing total rating).
* Table to Store details of book with author relation (with a field for storing total rating),
* Single table to store reviews about author and books (with a field for storing total rating).

**<u>APIs (With authentication)</u>**

* For Creating and listing authors (author object must contain number of books written by the author.)
* For updating and listing books
* For reviewing author and books (with rating) (creating a rating must add the total rating of author or book and store the average rating in the respective table.)
* For listing reviews of a particular author.

**<u>Optional:</u>**

 * Dockerize the application.



## Installation

To set up the project, create a PostgreSQL database named `book_management_dev` with the user `book_management_dev_user` and password `12345`, then follow the provided steps in the README.

* Create Environment
 ```
 python3 -m venv env
 ```

* Activate Environment
 
 on windows
 ```
 env\Scripts\activate
 ```
  on ubuntu
 ```
 source env/bin/activate
 ```

* Install Requirements

 ```
 pip install -r requirements.txt
 ```

* Migrate 
```
python3 manage.py migrate
```

* Create Admin SuperUser
```
python3 manage.py createsuperuser
```

* Runserver
```
python3 manage.py runserver
```


### APIs (With authentication)


Admin Authentication Required On :

    * Authors CRUD
    * Books CRUD

for creating admin users

```
python3 manage.py createsuperuser
```


1. Login & Signup (normal users)

    **POST**
    ```
    /users/login/
    ```
    ```
    {
        "username": "string",
        "password": "string",
    }
    ```

    **POST**
    ```
    /users/signup/
    ```
    ```
    {
        "username": "string",
        "password": "string"
    }
    ```


2. Authors CRUD 

    **POST & PATCH**

    ```
    /authors/author/
    ```

    ```
    {
    "first_name": "fname",
    "middle_name": "",
    "last_name": "lname",
    "bio": "string",
    "date_of_birth": "2024-02-13",
    "date_of_death": "2024-02-13",
    "nationality": "India",
    "contact_info": "string",
    "education_info": "string"
    }
    ```

    **GET & DELETE**
    ```
    /authors/author/
    ```
    ```
    /authors/author/1/
    ```
    ```
    [
        {
        "id": 0,
        "name": "string",
        "created_at": "2019-08-24T14:15:22Z",
        "updated_at": "2019-08-24T14:15:22Z",
        "bio": "string",
        "photo": "http://example.com",
        "date_of_birth": "2019-08-24",
        "date_of_death": "2019-08-24",
        "nationality": "string",
        "contact_info": "string",
        "education_info": "string",
        "total_rating": 0,
        "avg_rating": 0,
        "no_of_books": 0
        }
    ]
    ```

3. Books CRUD

    **POST & PATCH**
    ```
    /books/book/
    ```
    ```
    {
        "title": "string",
        "description": "string",
        "language": "string",
        "publisher": "string",
        "publication_dt": "2019-08-24T14:15:22Z",
        "total_pages": 10,
        "author": 1
    }
    ```

    **GET & DELETE**
    ```
    /books/book/1/
    ```

4. User Review (Author) CRUD

    **POST**
    ```
    /reviews/user-review/author/
    ```
    ```
    {
        "rating": 2,
        "review": "string",
        "author": 1
    }
    ```

    **GET & DELETE**
    ```
    /reviews/user-review/author/
    ```
    ```
    /reviews/user-review/author/1/
    ```
    ```
    {
    "status": 200,
    "message": "Ok!",
    "data": [
        {
            "id": 1,
            "author": {
                "id": 1,
                "name": "fname lname",
                "created_at": "2024-02-13T18:01:56.881947+05:30",
                "updated_at": "2024-02-13T18:32:02.613695+05:30",
                "bio": "string",
                "photo": null,
                "date_of_birth": "2024-02-13",
                "date_of_death": "2024-02-13",
                "nationality": "India",
                "contact_info": "string",
                "education_info": "string",
                "total_rating": 1,
                "avg_rating": 2.0,
                "no_of_books": 1
            },
            "created_at": "2024-02-13T18:32:02.610987+05:30",
            "updated_at": "2024-02-13T18:32:02.610998+05:30",
            "rating": 2.0,
            "review": "string",
            "user": 1
        }
    ]
}
    ```

5. User Review (Book) CRUD

    **POST**
    ```
    /reviews/user-review/book/
    ```
    ```
    {
        "rating": 0,
        "review": "string",
        "book": 0
    }
    ```

    **GET & DELETE**
    ```
    /reviews/user-review/book/
    ```
    ```
    /reviews/user-review/book/1/
    ```
    ```
    {
    "status": 200,
    "message": "Ok!",
    "data": [
            {
                "id": 2,
                "book": {
                    "id": 1,
                    "created_at": "2024-02-13T18:22:10.323359+05:30",
                    "updated_at": "2024-02-13T18:37:29.959449+05:30",
                    "title": "string",
                    "description": "string",
                    "cover_image": null,
                    "language": "string",
                    "ISBN": "8a3178",
                    "publisher": "string",
                    "publication_dt": "2019-08-24T19:45:22+05:30",
                    "total_pages": 5,
                    "total_rating": 1,
                    "avg_rating": 2.0,
                    "author": 1
                },
                "created_at": "2024-02-13T18:37:29.952122+05:30",
                "updated_at": "2024-02-13T18:37:29.952138+05:30",
                "rating": 2.0,
                "review": "string",
                "user": 1
            },
            {
                "id": 1,
                "book": {
                    "title": "",
                    "description": "",
                    "cover_image": null,
                    "language": "",
                    "ISBN": "",
                    "publisher": "",
                    "publication_dt": null,
                    "total_pages": null,
                    "total_rating": null,
                    "avg_rating": null,
                    "author": null
                },
                "created_at": "2024-02-13T18:32:02.610987+05:30",
                "updated_at": "2024-02-13T18:32:02.610998+05:30",
                "rating": 2.0,
                "review": "string",
                "user": 1
            }
        ]
    }
    ```

6. Reviews 

    **GET**

    all reviews

    ```
    /reviews/
    ```

    author based reviews

    ```
    /reviews/?author=6
    ```

    book based reviews

    ```
    /reviews/?book=6
    ```

## API Docs

You can access the Swagger documentation
```
http://127.0.0.1:8000/docs/
``` 
```
http://127.0.0.1:8000/redoc/ 
``` 


